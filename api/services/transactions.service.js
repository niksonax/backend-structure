const { UserDoesntExistsError } = require("../../data/errors/errors");
const transactionRepository = require("../../data/repositories/transaction.repository");
const userRepository = require("../../data/repositories/user.repository");

const createTransaction = async (payload) => {
  const [user] = await userRepository.getUserById(payload.userId);
  if (!user) {
    throw new UserDoesntExistsError("User does not exist");
  }

  const { id, amount, cardNumber: card_number, userId: user_id } = payload;
  const updatedPayload = {
    id,
    user_id,
    card_number,
    amount,
  };

  const [transaction] = await transactionRepository.createTransaction(
    updatedPayload
  );

  const {
    user_id: userId,
    card_number: cardNumber,
    created_at: createdAt,
    updated_at: updatedAt,
    ...rest
  } = transaction;

  const currentBalance = amount + user.balance;

  const [updatedUser] = await userRepository.updateBalance(
    user.id,
    currentBalance
  );

  const updatedTransaction = {
    userId,
    ...rest,
    cardNumber,
    createdAt,
    updatedAt,
    currentBalance,
  };

  return updatedTransaction;
};

module.exports = { createTransaction };
