const jwt = require("jsonwebtoken");
const { UserNotFoundError } = require("../../data/errors/errors.js");
const userRepository = require("../../data/repositories/user.repository.js");

const getUserById = async (id) => {
  const data = await userRepository.getUserById(id);
  if (data.length === 0) {
    throw new UserNotFoundError("User not found");
  }
  const [user] = data;
  return user;
};

const createUser = async (payload) => {
  const data = await userRepository.createUser(payload);
  const [{ created_at: createdAt, updated_at: updatedAt, ...rest }] = data;
  const newUser = {
    createdAt,
    updatedAt,
    accessToken: jwt.sign(
      { id: rest.id, type: rest.type },
      process.env.JWT_SECRET
    ),
    ...rest,
  };
  return newUser;
};

const updateUser = async (id, payload) => {
  const data = await userRepository.updateUser(id, payload);
  const [user] = data;
  return user;
};

module.exports = { getUserById, createUser, updateUser };
