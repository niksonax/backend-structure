module.exports = (err, req, res, next) => {
  console.log("Error Handler: ", err.name);

  if (!err.status) {
    res.status(500).send("Internal Server Error");
    return;
  }

  res.status(err.status).send({ error: err.message });
};
