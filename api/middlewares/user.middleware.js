const joi = require("joi");
const jwt = require("jsonwebtoken");
const {
  InvalidFormatError,
  InvalidIdError,
  NotAuthorizedError,
  UserIdMismatchError,
} = require("../../data/errors/errors");

const getUserMiddleware = (req, res, next) => {
  const schema = joi
    .object({
      id: joi.string().uuid(),
    })
    .required();
  const isValidResult = schema.validate(req.params);
  if (isValidResult.error) {
    throw new InvalidIdError(isValidResult.error.details[0].message);
  }
  next();
};

const postUserMiddleware = (req, res, next) => {
  const schema = joi
    .object({
      id: joi.string().uuid(),
      type: joi.string().required(),
      email: joi.string().email().required(),
      phone: joi
        .string()
        .pattern(/^\+?3?8?(0\d{9})$/)
        .required(),
      name: joi.string().required(),
      city: joi.string(),
    })
    .required();
  const isValidResult = schema.validate(req.body);
  if (isValidResult.error) {
    throw new InvalidFormatError(isValidResult.error.details[0].message);
  }
  next();
};

const putUserMiddleware = (req, res, next) => {
  let token = req.headers["authorization"];
  let tokenPayload;
  if (!token) {
    throw new NotAuthorizedError("Not Authorized");
  }
  token = token.replace("Bearer ", "");
  try {
    tokenPayload = jwt.verify(token, process.env.JWT_SECRET);
  } catch (err) {
    throw new NotAuthorizedError("Not Authorized");
  }
  var schema = joi
    .object({
      email: joi.string().email(),
      phone: joi.string().pattern(/^\+?3?8?(0\d{9})$/),
      name: joi.string(),
      city: joi.string(),
    })
    .required();
  var isValidResult = schema.validate(req.body);
  if (isValidResult.error) {
    throw new InvalidFormatError(isValidResult.error.details[0].message);
  }
  if (req.params.id !== tokenPayload.id) {
    throw new UserIdMismatchError("UserId mismatch");
  }
  next();
};

module.exports = { getUserMiddleware, postUserMiddleware, putUserMiddleware };
