const jwt = require("jsonwebtoken");
const { NotAuthorizedError } = require("../../data/errors/errors");

module.exports = (req, res, next) => {
  let token = req.headers["authorization"];
  if (!token) {
    throw new NotAuthorizedError("Not Authorized");
  }
  token = token.replace("Bearer ", "");
  try {
    const tokenPayload = jwt.verify(token, process.env.JWT_SECRET);
    if (tokenPayload.type != "admin") {
      throw new NotAuthorizedError("Not Authorized");
    }
  } catch (err) {
    throw new NotAuthorizedError("Not Authorized");
  }
  next();
};
