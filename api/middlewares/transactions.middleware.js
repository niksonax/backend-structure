const joi = require("joi");
const jwt = require("jsonwebtoken");
const {
  InvalidFormatError,
  NotAuthorizedError,
} = require("../../data/errors/errors");

module.exports = (req, res, next) => {
  const schema = joi
    .object({
      id: joi.string().uuid(),
      userId: joi.string().uuid().required(),
      cardNumber: joi.string().required(),
      amount: joi.number().min(0).required(),
    })
    .required();
  const isValidResult = schema.validate(req.body);
  if (isValidResult.error) {
    throw new InvalidFormatError(isValidResult.error.details[0].message);
  }

  let token = req.headers["authorization"];
  if (!token) {
    throw new NotAuthorizedError("Not Authorized");
  }
  token = token.replace("Bearer ", "");
  try {
    const tokenPayload = jwt.verify(token, process.env.JWT_SECRET);
    if (tokenPayload.type != "admin") {
      throw new Error();
    }
  } catch (err) {
    throw new NotAuthorizedError("Not Authorized");
  }
  next();
};
