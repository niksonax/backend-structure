const express = require("express");
const userServices = require("../services/users.service");

const userMiddleware = require("../middlewares/user.middleware");
const errorHandlerMiddleware = require("../middlewares/error-handle.middleware");

const router = express.Router();

const db = require("../../db/connection");

router.get(
  "/:id",
  userMiddleware.getUserMiddleware,
  async (req, res, next) => {
    try {
      const data = await userServices.getUserById(req.params.id);
      res.send(data);
    } catch (err) {
      next(err);
      return;
    }
  },
  errorHandlerMiddleware
);

router.post(
  "/",
  userMiddleware.postUserMiddleware,
  async (req, res, next) => {
    req.body.balance = 0;
    try {
      const data = await userServices.createUser(req.body);
      res.send(data);
    } catch (err) {
      next(err);
    }
  },
  errorHandlerMiddleware
);

router.put(
  "/:id",
  userMiddleware.putUserMiddleware,
  async (req, res, next) => {
    try {
      const data = await userServices.updateUser(req.params.id, req.body);
      res.send(data);
    } catch (err) {
      next(err);
    }
  },
  errorHandlerMiddleware
);

module.exports = router;
