const healthRoutes = require("./health.routes.js");
const usersRoutes = require("./users.routes.js");
const transactionsRoutes = require("./transactions.routes.js");
const statsRoutes = require("./stats.routes.js");

module.exports = (app) => {
  app.use("/health", healthRoutes);
  app.use("/users", usersRoutes);
  app.use("/transactions", transactionsRoutes);
  app.use("/stats", statsRoutes);
};
