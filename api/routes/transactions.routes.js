const express = require("express");
const errorHandleMiddleware = require("../middlewares/error-handle.middleware");
const transactionsMiddleware = require("../middlewares/transactions.middleware");
const { createTransaction } = require("../services/transactions.service");

const router = express.Router();

router.post(
  "/",
  transactionsMiddleware,
  async (req, res, next) => {
    try {
      const transaction = await createTransaction(req.body);
      return res.send(transaction);
    } catch (err) {
      next(err);
    }
  },
  errorHandleMiddleware
);

module.exports = router;
