const Router = require("express");
const stats = require("../services/stats.service");
const statsMiddleware = require("../middlewares/stats.middleware");
const errorHandlerMiddleware = require("../middlewares/error-handle.middleware");

const router = Router();

router.get(
  "/",
  statsMiddleware,
  (req, res) => {
    try {
      res.send(stats);
    } catch (err) {
      next(err);
    }
  },
  errorHandlerMiddleware
);

module.exports = router;
