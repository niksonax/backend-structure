class InternalServerError extends Error {
  constructor(message) {
    super(message);
    this.name = "InternalServerError";
    this.status = 500;
  }
}

class AlreadyExistsError extends Error {
  constructor(message) {
    super(message);
    this.name = "AlreadyExists";
    this.status = 400;
  }
}

class InvalidFormatError extends Error {
  constructor(message) {
    super(message);
    this.name = "InvalidFormat";
    this.status = 400;
  }
}

class InvalidIdError extends Error {
  constructor(message) {
    super(message);
    this.name = "InvalidId";
    this.status = 400;
  }
}

class UserNotFoundError extends Error {
  constructor(message) {
    super(message);
    this.name = "UserNotFound";
    this.status = 404;
  }
}

class UserDoesntExistsError extends Error {
  constructor(message) {
    super(message);
    this.name = "UserDoesntExists";
    this.status = 400;
  }
}

class NotAuthorizedError extends Error {
  constructor(message) {
    super(message);
    this.name = "NotAuthorized";
    this.status = 401;
  }
}

class UserIdMismatchError extends Error {
  constructor(message) {
    super(message);
    this.name = "UserIdMismatch";
    this.status = 401;
  }
}

module.exports = {
  InternalServerError,
  AlreadyExistsError,
  InvalidFormatError,
  InvalidIdError,
  UserNotFoundError,
  UserDoesntExistsError,
  NotAuthorizedError,
  UserIdMismatchError,
};
