const db = require("../../db/connection");
const { AlreadyExistsError, InternalServerError } = require("../errors/errors");

class UserRepository {
  async getUserById(id) {
    const user = await db("user").where("id", id).returning("*");
    return user;
  }

  async createUser(payload) {
    const newUser = await db("user")
      .insert(payload)
      .returning("*")
      .catch((err) => {
        if (err.code === "23505") {
          throw new AlreadyExistsError(err.detail);
        }
        throw new InternalServerError("Internal Server Error");
      });
    return newUser;
  }

  async updateUser(id, payload) {
    const updatedUser = await db("user")
      .where("id", id)
      .update(payload)
      .returning("*")
      .catch((err) => {
        if (err.code == "23505") {
          throw new AlreadyExistsError(err.detail);
        }
        throw new InternalServerError("Internal Server Error");
      });
    return updatedUser;
  }

  async updateBalance(id, balance) {
    const updatedUser = await db("user")
      .where("id", id)
      .update("balance", balance)
      .returning("*");
    return updatedUser;
  }
}

const userRepository = new UserRepository();
module.exports = userRepository;
