const db = require("../../db/connection");

class TransactionRepository {
  async createTransaction(payload) {
    const transaction = await db("transaction").insert(payload).returning("*");
    return transaction;
  }
}

const transactionRepository = new TransactionRepository();
module.exports = transactionRepository;
